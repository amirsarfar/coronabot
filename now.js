{
  "builds": [{ 
    "src": "coronabot.py", "use": "@now/python"
  }],
  "routes": [
		{ "src": "/(.*)", "dest": "coronabot.py" }
  ],
  "env": {
    "TELEGRAM_TOKEN": "@telegram-token"
  }
}